//create many spheres at random positions, add to grids.
//for a large set of input points, brute force check for collision inside each sphere.
//using grid system, check that all spheres found are inside 

var GRID_DIVS=8;

var timeFunc = window.performance;  //or Date

function doBigTest(){
    var gridListLen = GRID_DIVS*GRID_DIVS*GRID_DIVS*8;

    var gridLists = new Array(gridListLen);

    //initialise gridLists
    for (var ii=0;ii<gridListLen;ii++){
        gridLists[ii]=[];
    }

    var numSpheres = 1000;
    var sphereArray = new Array(numSpheres);
    for (var ii=0;ii<numSpheres;ii++){

        var radius = Math.random()*0.2 + 0.01;
        var thisSphere = {
            radius,
            position:rand4vec(),
            critDotProd:1/Math.sqrt(1+radius*radius)
        }

        sphereArray[ii] = thisSphere;

        //add to grids
        var setOfGridSquares = getGridId.forSphere(thisSphere.position, thisSphere.radius);
        var arrayOfGridSquares = Array.from(setOfGridSquares);

        //add this sphere id to each grid square in list
        for (var jj=0;jj<arrayOfGridSquares.length;jj++){
            gridLists[arrayOfGridSquares[jj]].push(ii);
        }
    }

    console.log(gridLists);


    //create a set of test points. 
    var numProbes = 1000;
    var probeArray = new Array(numProbes);
    for (var ii=0;ii<numProbes;ii++){
        probeArray[ii] = rand4vec();
    }

    //for each probe, check vs every sphere.
    //just count collisions for basic sanity check
    var numCollisionsBruteForce = 0;
    var startTime = timeFunc.now();
    for (var ii=0;ii<numProbes;ii++){
        for (var jj=0;jj<numSpheres;jj++){
            numCollisionsBruteForce += probeSphereCollision(probeArray[ii],sphereArray[jj]);
        }
    }
    var endTime = timeFunc.now();
    var bruteForceTime = endTime-startTime;
    startTime=endTime;

    //repeat by finding grid idx of cube and just checking vs spheres there.
    var numCollisionsFastMethod = 0;
    for (var ii=0;ii<numProbes;ii++){
        var probeGridIdx = getGridId.forPoint(probeArray[ii]);
        var sphereListForGrid = gridLists[probeGridIdx];
        for (var jj=0;jj<sphereListForGrid.length;jj++){
            numCollisionsFastMethod+=probeSphereCollision(probeArray[ii],sphereArray[sphereListForGrid[jj]]);
        }
    }
    var endTime = timeFunc.now();
    var fastMethodTime = endTime-startTime;

    console.log({
        numCollisionsBruteForce,
        bruteForceTime,
        numCollisionsFastMethod,
        fastMethodTime
    });


}

function probeSphereCollision(probe, sphere){
    var spherePos = sphere.position;
    var dotProd = probe[0]*spherePos[0] + probe[1]*spherePos[1] + probe[2]*spherePos[2] + probe[3]*spherePos[3];

        //critical is cos(angle). radius is tan(angle)
        //c^2 + s^2 = 1
        // 1 + t^2 = 1/c^2
        // 1/ root(1+t^2)
    var critDotProd = sphere.critDotProd;
    return dotProd>critDotProd ? 1 : 0;
}