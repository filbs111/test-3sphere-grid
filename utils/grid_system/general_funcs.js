function rand4vec(){
    var randVec = new Array(4);
    
    for (var cc=0;cc<4;cc++){
        randVec[cc] = Math.random()-0.5;
    }
    
    //normalise coords
    var lensq=0;
    for (var cc=0;cc<4;cc++){
        lensq+=randVec[cc]*randVec[cc];
    }
    var len = Math.sqrt(lensq);
    for (var cc=0;cc<4;cc++){
        randVec[cc]/=len;
    }

    return randVec;
}