var getGridId=(function generateGetGridId(gridDivs){
    
    var halfGridDivs = gridDivs/2;
    var piBy4 = Math.PI/4;

    function normalise4vec(vectonormalise){
        //normalise coords
        var lensq=0;
        for (var cc=0;cc<4;cc++){
            lensq+=vectonormalise[cc]*vectonormalise[cc];
        }
        var len = Math.sqrt(lensq);
        for (var cc=0;cc<4;cc++){
            vectonormalise[cc]/=len;
        }
    }


    var orthogPart = new Array(4);
    var randVec = new Array(4);
    function randDisplaced4Vec(centrecoord, sphereRad){

        var endVec = new Array(4);

        //random 4 vector.
        for (var cc=0;cc<4;cc++){
            randVec[cc] = Math.random()-0.5;
        }
        var dotProd =0;
        for (var cc=0;cc<4;cc++){
            dotProd+=randVec[cc]*centrecoord[cc];
        }

        for (var cc=0;cc<4;cc++){
            orthogPart[cc]=randVec[cc] - dotProd*centrecoord[cc];
        }

        lensq=0;
        for (var cc=0;cc<4;cc++){
            lensq+=orthogPart[cc]*orthogPart[cc];
        }
        var len = Math.sqrt(lensq);

        var fractionFromCentre = Math.random(); //1 for all points on surface, good for calculating ranges. but to hit grid squares inside sphere stochastically,
                        //want this to be random. note this is inefficient as linear distribution since most points near centre.

        for (var cc=0;cc<4;cc++){
            endVec[cc] = centrecoord[cc]+fractionFromCentre*sphereRad*orthogPart[cc]/len;
        }

        return endVec;
    }

    function get3dGridNum(projectedCoords){
        var gridNums = new Array(3);
        for (var cc=0;cc<3;cc++){
            var thisGridNum = Math.floor(halfGridDivs*(projectedCoords[cc]+1));
            if (thisGridNum >= gridDivs){console.log("hit max");}
            thisGridNum = Math.min(thisGridNum, gridDivs-1);
            gridNums[cc] = thisGridNum;
        }
        return gridNums;
    }

    function get3dAngleGridNum(localcoords){
        var atan2GridNums = new Array(3);
        for (var cc=0;cc<3;cc++){
            var atan2GridNum = Math.atan2(localcoords[cc+1], localcoords[0]);
            atan2GridNum = Math.floor(halfGridDivs* (atan2GridNum/piBy4 +1));
            if (atan2GridNum >= gridDivs){console.log("hit max atan2");}
            if (atan2GridNum <0){console.log("hit min atan2");}
            atan2GridNum = Math.min(atan2GridNum, gridDivs-1);
            atan2GridNum = Math.max(atan2GridNum, 0);
            atan2GridNums[cc] = atan2GridNum;
        }
        return atan2GridNums;
    }

    function get3dAngleGridNumMinMax(localcoords, sphereRad){
        var atan2GridNumMinMax = new Array(3);
        for (var cc=0;cc<3;cc++){
            var atan2GridNum = Math.atan2(localcoords[cc+1], localcoords[0]);

            var vsq = localcoords[0]*localcoords[0] + localcoords[cc+1]*localcoords[cc+1];  // or 1- this??
            var wsq = 1-vsq;    //= coords[2]*coords[2] + coords[3]*coords[3], if coords normalised
            var plusMinusTanAng = sphereRad / Math.sqrt( vsq - (sphereRad*sphereRad*wsq));
                    //this basically works, though falls over when part inside sqrt goes -ve.
            var plusMinusAng = Math.atan(plusMinusTanAng);

            var maxAng = atan2GridNum + plusMinusAng;
            var minAng = atan2GridNum - plusMinusAng;

            var maxGridNum = Math.floor(halfGridDivs* (maxAng/piBy4 +1));
            var minGridNum = Math.floor(halfGridDivs* (minAng/piBy4 +1));

            maxGridNum+=1;  //range is from minGridNum to maxGridNum+1

            // if (maxGridNum > gridDivs){console.log("hit max atan2");}
            // if (maxGridNum <0){console.log("hit min atan2");}

            // if (minGridNum > gridDivs){console.log("hit max atan2");}
            // if (minGridNum <0){console.log("hit min atan2");}

            maxGridNum = Math.min(maxGridNum, gridDivs);
            maxGridNum = Math.max(maxGridNum, 0);

            minGridNum = Math.min(minGridNum, gridDivs);
            minGridNum = Math.max(minGridNum, 0);

            atan2GridNumMinMax[cc] = {max:maxGridNum, min:minGridNum};
        }
        return atan2GridNumMinMax;
    }

    var absArr = new Array(4);

    var getGridId = function getGridId(coords){ //coords = [w,x,y,z]
        
        var lwIdx;
        var localIdx=new Array(4);
        var localcoords=new Array(4);
        var projectedCoords = new Array(3);
        var gridIdx;

        for (var cc=0;cc<4;cc++){
            absArr[cc]=Math.abs(coords[cc]);
        }

        //this part maybe has some cunning trick to make more efficient
        lwIdx = 0;
        for (var cc=1;cc<4;cc++){
            lwIdx = absArr[cc] > absArr[lwIdx]? cc:lwIdx;
        }
        
        isNegativeShift = coords[lwIdx]<0 ? 4:0;

        localIdx[0] = lwIdx + isNegativeShift;
        localcoords[0] = absArr[lwIdx];
        for (var cc=1;cc<4;cc++){
            localIdx[cc] = (lwIdx + cc + isNegativeShift) & 7; // which element of array [w,x,y,z.-w,-x,-y,-z]
            localcoords[cc] = coords[localIdx[cc] & 3] * ( localIdx[cc] < 4 ? 1:-1 );
            projectedCoords[cc-1] = localcoords[cc]/localcoords[0];
        }
        //note projected coords would work better if w used array position 3 instead of 0

        //simple logging of results
        // console.log("input coords:", coords);
        // console.log("cell id: ", localIdx[0]);
        // console.log("projected coords:", projectedCoords);

        //get grid id
        var gridNums = get3dGridNum(projectedCoords);
        var atan2GridNums = get3dAngleGridNum(localcoords);

        //get gridIdx
        gridIdx = gridNums[0] + gridDivs*( gridNums[1] + gridDivs*gridNums[2] );    
        gridIdx = atan2GridNums[0] + gridDivs*( atan2GridNums[1] + gridDivs*atan2GridNums[2] );

        // console.log("gridnums: ", gridNums);
        // console.log("atan2gridnums: ", atan2GridNums);
        // console.log("grididx: ", gridIdx);
        // console.log("atan2grididx: ", gridIdx);

        //TODO list neighbouring squares. don't need this for all use cases.

        gridIdx+= gridDivs*gridDivs*gridDivs * (lwIdx + isNegativeShift);   //shift for cell. (ie cell 0 is grid indices from 0 to gridDivs^3 -1, etc)

        return gridIdx;
    }

    var testAtan2Ranges = function(coords){ //coords = [w,x,y,z]

        //get range of atan2 numbers (for all 6 combinations of axes), for some sphere of given radius (before projection onto 3-sphere)
        
        //to check code for this, lookup this value (atan2) for a (large) set of points on/in sphere of given radius around the input point.
        //the range of these points should tend to the calculated value as number of points tends to infinity.

        //for a given cell, only 3 are relevant, but for now just calculate one, see if calculation works
        //above, worked out relevant axes for input point, but complicated by sphere about point crossing into different cells.
        //therefore for now hard code which atan2 value calculating.

        var centreAtan2Val = Math.atan2(coords[0],coords[1]);   // (w,x)
            //this has sense near y=z=0 line, applies for 4 cells that this line passes through 

        //repeat this for large series of points 
        var angleMaxPlus=0;
        var angleMaxMinus=0;

        var predictedTanAng;
        var predictedAng;

        var sphereRad=0.5;

        normalise4vec(coords);

        for (var ii=0;ii<100000;ii++){
            
            var endVec = randDisplaced4Vec(coords, sphereRad);

            var thisAtan2Vec = Math.atan2(endVec[0],endVec[1]);

            var differenceAng = thisAtan2Vec - centreAtan2Val;
            
            //TODO does this happen?
            if (differenceAng>Math.PI){differenceAng-=2*Math.PI;}
            if (differenceAng<-Math.PI){differenceAng+=2*Math.PI;}

            angleMaxPlus = Math.max(angleMaxPlus, differenceAng);
            angleMaxMinus = Math.min(angleMaxMinus, differenceAng);

            //prediction of this angle from notes on paper.
            // tan-1( r/sqrt(v^2 - (r/w)^2))
            var vsq = coords[0]*coords[0] + coords[1]*coords[1];
            var wsq = 1-vsq;    //= coords[2]*coords[2] + coords[3]*coords[3], if coords normalised
            predictedTanAng = sphereRad / Math.sqrt( vsq - (sphereRad*sphereRad*wsq));
                    //this basically works, though falls over when part inside sqrt goes -ve.
            predictedAng = Math.atan(predictedTanAng);

        }
        
        var decimalPoints = 5;
        console.log({
            centreAtan2Val:centreAtan2Val.toPrecision(decimalPoints),
            angleMaxPlus:angleMaxPlus.toPrecision(decimalPoints), 
            angleMaxMinus:angleMaxMinus.toPrecision(decimalPoints),
            predictedAng:predictedAng.toPrecision(decimalPoints),       //should match maxplus, maxminus
            //predictedTanAng:predictedTanAng.toPrecision(decimalPoints)
        });

        //TODO get set of grid squares covering this range of angles.
        //then should get any grid squares in neighbouring cells. this could be up to 3 (assuming sphere fairly small) cells, of the 6 surrounding cells.
        //maybe it's just easier to check for all 8 cells independently. or all 4 pairs of opposite cells.

        //basically this is run the 1st code part with 4 different values of lwIdx, finding grid squares in each cell.

        //start by just getting set of grid squares for one cell.
        //say +w cell. can check results here vs results for grid cells for large number of random points in sphere.
    }

    var getGridIndicesForSphereStochastic = function(centrecoord, sphereRad){
        normalise4vec(centrecoord);
        var setOfIdxs = new Set();
        for (var ii=0;ii<1000000;ii++){
            setOfIdxs.add(getGridId(randDisplaced4Vec(centrecoord,sphereRad)));
        }
        return setOfIdxs;
    }

    var getGridIndicesForSphereByLimits = function(centrecoord, sphereRad){

        //shift by cell number
        var gridDivsCubed = gridDivs*gridDivs*gridDivs;

        for (var cc=0;cc<4;cc++){
            absArr[cc]=Math.abs(centrecoord[cc]);
        }

        var localIdx = new Array(4);
        var localcoords = new Array(4);
        var gridIndices = new Set();

        for (var lwIdx=0;lwIdx<4;lwIdx++){
            //assumes that sphere is small enough to only hit maximum of 4 cells at a time. falls over for larger spheres that can hit 2 opposite cells simultaneously.
                //happens for sphereRad 1 in centre, or sphereRad 1/root2 halfway along edge. 

            isNegativeShift = centrecoord[lwIdx]<0 ? 4:0;

            localIdx[0] = lwIdx + isNegativeShift;
            localcoords[0] = absArr[lwIdx];
            for (var cc=1;cc<4;cc++){
                localIdx[cc] = (lwIdx + cc + isNegativeShift) & 7; // which element of array [w,x,y,z.-w,-x,-y,-z]
                localcoords[cc] = centrecoord[localIdx[cc] & 3] * ( localIdx[cc] < 4 ? 1:-1 );
            }

            cellShift = gridDivsCubed* localIdx[0];

            var minMax = get3dAngleGridNumMinMax(localcoords, sphereRad);
            //console.log(minMax);
            
            for (var aa=minMax[0].min; aa<minMax[0].max; aa++){
                for (var bb=minMax[1].min; bb<minMax[1].max; bb++){
                    for (var cc=minMax[2].min; cc<minMax[2].max; cc++){
                        gridIndices.add(aa + gridDivs*(bb + gridDivs*cc) + cellShift );
                    }
                }
            }
        }
        
        console.log(gridIndices);

        return gridIndices;
    }

    var testIndicesForSphere = function(centrecoord, sphereRad){
        var stochasticIndices = getGridIndicesForSphereStochastic(centrecoord, sphereRad);
        var bylimitsIndices = getGridIndicesForSphereByLimits(centrecoord, sphereRad);

        //check that all elements of set stochasticIndices are included in bylimitsIndices
        var sizeRatio = bylimitsIndices.size / stochasticIndices.size;
        console.log("bylimitsIndices.size / stochasticIndices.size = " + sizeRatio + " ( should be >=1 )");
        if (sizeRatio < 1){
            console.log("unexpectedly, stochasticIndices.size > bylimitsIndices.size ");
        }
        var combinedSet = new Set([...stochasticIndices, ...bylimitsIndices]);

        console.log({stochasticIndices, bylimitsIndices, combinedSet});

        if (combinedSet.size == bylimitsIndices.size){
            console.log("combined set equal size to bylimitsindices, as expected");
        }else{
            console.log("problem! combinedSet.size != bylimitsIndices.size , so some members of stochasticIndices missing from bylimitsIndices");
                //can see problem by getGridId.testIndicesForSphere([1,0,0,0],1.001) , expected since bylimitsIndices currently only found for +w cell
        }
    }

    return {
        forPoint:getGridId,
        forSphere:getGridIndicesForSphereByLimits,
        forSphereStochastic:getGridIndicesForSphereStochastic,
        testIndicesForSphere,
        testAtan2Ranges
    }

})(GRID_DIVS);