grid system to make collision detection etc more efficient. particularly relevant to 600-cell collision vs bullets, which currently runs collision every mechanics iteration for every bullet-cell pair. 600 static tetrahedral frame objects, ~100 bullets. this results in severe slowdown, even on a fast machine.

expect a gridding system will reduce slowdown. simple system: for each grid square, make a list, possibly at loading time, of all static objects that might be collided with an some object with some maximum radius, with centre somewhere inside that grid square. The simple case, for radius 0, is suitable for bullets. For larger, or moving objects, either create object lists for larger radius, or calculate axis-aligned bounding box, and find which grid squares are covered. This maybe a good fit with tree system (eg octree).

4D grid is relatively simple. basic 2x2x2x2 grid (16 cells) should provide near 16x speedup over no grid. However for larger grid, 8 3d grids, one for each 8-cell cell, projected onto the 3-sphere, seems more sensible. There are fewer grid squares for a given grid density in 3D local space on 3-sphere surface than 4D grid (o(3) vs o(4)), though this might not be a serious issue for sparse tree. Also, for 3d grid, great circles become straight lines when projected from 3-sphere onto 3D grid, so can assume that polygonal object is inside AABB defined by maximum/minimum of all verts. Can't do this for surface of 3-sphere and 4D grid. Might do this for 3D objects before projection onto 3-sphere, but then colliding shapes are conic prisms, so need to check between grid tesseracts that overlap when viewed from the origin. Perhaps this is viable, but 8x 3d grids is IMO easier to think about.

One dimension down, can consider case of collisions on surface of sphere. Proposed solution is to use cube grid projected onto sphere. 4D grid solution equivalent is 3D grid. We can use a 3D grid if we add static objects to list for all cubes that contain some part of the conic prism from origin to object on surface of sphere. might do this by finding 3D bounding box of object before projection onto sphere, checking whether this viewed from origin intersects with another box. Alternatively, add object to boxes before projection, no prism (objects typically assigned to grid boxes near sphere surface), then at runtime, run a ray from origin to colliding object, look up every box intersected.

Expect projected cube grid version to be more efficient. TODO try both methods. Maybe some trick to make the "naive" method faster. FWIW, conic prism objects colliding with some box is same for another box scaled about origin, eg by factor 2. 

Another use for grid is for collision checks between moving objects. 

Might want grid to have property that, for a given 3D grid (1 for each 8-cell cell), for given position in a local coordinate system, the difference between the id of a given grid square and those of its neighbours in each direction, is the same as for the other 7 cells. This will be useful if want a simple system for collisions between moving objects, where use grid squares bigger than largest object radius, assign objects to grids, check collisions for objects in same and neighbouring grid squares. therefore want way to easily look up neighbouring grid squares. Alternatively can just list neighbours for each square, but if can calculate simply, saves space.

8 cells. +w,+x,+y,+z,-w,-x,-y,-z

cell +w. centre: (1,0,0,0), corners: ½ (1, ±1, ±1, ±1)

cell +x. centre: (0,1,0,0), corners: ½ (±1, 1, ±1, ±1)

...

cell -w. centre: (-1,0,0,0), corners: ½ (-1, ±1, ±1, ±1)

...

in each define local 3D axes. each varies monotonically with one of the 4D axes.

for cell +w, local x,y,z axes vary with 4D x,y,z axes.

for cell +x, local x,y,z axes vary with 4D y,z,-w axes (local z increases as 4D w decreases)

|grid cell (local w)|centre|corners|local x|local y|local z|
|---------|------|-------|-------|-------|-------|
|+w|(1,0,0,0)|½ (1, ±1, ±1, ±1)|+x|+y|+z|
|+x|(0,1,0,0)|½ (±1, 1, ±1, ±1)|+y|+z|-w|
|+y|(0,0,1,0)|½ (±1, ±1, 1, ±1)|+z|-w|-x|
|+z|(0,0,0,1)|½ (±1, ±1, ±1, 1)|-w|-x|-y|
|-w|(-1,0,0,0)|½ (-1, ±1, ±1, ±1)|-x|-y|-z|
|-x|(0,-1,0,0)|½ (±1, -1, ±1, ±1)|-y|-z|+w|
|-y|(0,0,-1,0)|½ (±1, ±1, -1, ±1)|-z|+w|+x|
|-z|(0,0,0,-1)|½ (±1, ±1, ±1, 1)|+w|+x|+y|

note diagonal pattern. can switch/change sign on components here and still works. choice is arbitrary.

For each cell, local id of cube inside that grid is dependent on local co-ordinates. For example, suppose each cell is 16x16x16. we can get co-ordinates from -1 to 1 for +w cell: (x/w, y/w, z/w). or, for more evenly sized grid cubes when projected onto 3-sphere, can do (atan2(x,w), atan2(y,w), atan2(z,w)). Then, with these new coordinates (xlocal,ylocal,zlocal), do, eg, gridx = floor( ½(xlocal+1)*16 ). 

To determine which cell a given point is in, can simply find which of w,x,y,z has largest absolute value. Which cell to assign corner/edge/surface cases to can be arbitrary, provided a gridx, gridy, gridz is capped to max value. It is possible to assign edge cases such that this is almost unneccesary, with the exception of 8 coordinates.

TODO check if efficient, when using atan2 gridding, to calculate these values first, then assign cube and calculate grid points using these values.